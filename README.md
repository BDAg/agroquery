# AgroQuery

<pre>Projeto voltado à coleta e exposição de dados sobre cotações de soja, café e milho.</pre>



<hr/>

### Wiki
[Wiki do projeto](https://gitlab.com/BDAg/agroquery/-/wikis/home)
<hr/>

### Equipe
[Daniel Vieira Mendes](https://gitlab.com/BDAg/agroquery/-/wikis/Daniel-Vieira-Mendes)<br>
[João Pedro Aguilar Vicari](https://gitlab.com/BDAg/agroquery/-/wikis/João-Pedro-Aguilar-Vicari)<br>
[Bryan Mauricio de Souza Francisco](https://gitlab.com/BDAg/agroquery/-/wikis/Bryan-Mauricio-de-Souza-Francisco)<br>
[Cesar Augusto Matos Ladeira](https://gitlab.com/BDAg/agroquery/-/wikis/Cesar-Augusto-Matos-Ladeira)<br>
[Lucas Eduardo Dessy](https://gitlab.com/BDAg/agroquery/-/wikis/Lucas-Eduardo-Dessy)