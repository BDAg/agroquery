import requests
from bs4 import BeautifulSoup as BF
from lxml import html

# ARABICA
Vazio = 0
i = 0
link = "https://www.noticiasagricolas.com.br/cotacoes/cafe/indicador-cepea-esalq-cafe-arabica"
req = requests.get(link)
tree = html.fromstring(req.content)

print("Cotações café Arábica:")

while(True):
    i += 1
    Data = tree.xpath('//*[@id="content"]/div[3]/div[3]/div[{}]/div[2]/table/tbody/tr/td[1]/text()'.format(i))
    Preco = tree.xpath('//*[@id="content"]/div[3]/div[3]/div[{}]/div[2]/table/tbody/tr/td[2]/text()'.format(i))
    Variacao = tree.xpath('//*[@id="content"]/div[3]/div[3]/div[{}]/div[2]/table/tbody/tr/td[3]/text()'.format(i))

    if (Data != []): #Nao colocar dados em Branco
        Vazio = 0
        print("Data: ",Data[0]," | Preço: ",Preco[0]," | Variação: ",Variacao[0])
    else:
        Vazio += 1
    

    if (Vazio >= 3): #Verificar quando parar
        break


# Conillon
Vazio = 0
i = 0
link = "https://www.noticiasagricolas.com.br/cotacoes/cafe/indicador-cepea-esalq-cafe-conillon"
req = requests.get(link)
tree = html.fromstring(req.content)

print("\nCotações café Conillon:")

while(True):
    i += 1
    Data = tree.xpath('//*[@id="content"]/div[3]/div[3]/div[{}]/div[2]/table/tbody/tr/td[1]/text()'.format(i))
    Preco = tree.xpath('//*[@id="content"]/div[3]/div[3]/div[{}]/div[2]/table/tbody/tr/td[2]/text()'.format(i))
    Variacao = tree.xpath('//*[@id="content"]/div[3]/div[3]/div[{}]/div[2]/table/tbody/tr/td[3]/text()'.format(i))

    if (Data != []): #Nao colocar dados em Branco
        Vazio = 0
        print("Data: ",Data[0]," | Preço: ",Preco[0]," | Variação: ",Variacao[0])
    else:
        Vazio += 1
    

    if (Vazio >= 3): #Verificar quando parar
        break